INSERT INTO customers VALUES
('1','Lionel Messi'),
('2','Virgil van Dijk'),
('3','Bernardo Silva'),
('4','Sadio Mane'),
('5','Mohamed Salah'),
('6','Cristiano Ronaldo'),
('7','Raheem Sterling'),
('8','Frenkie de Jong'),
('9','Eden Hazard'),
('10','Alisson Becker'),
('11','Kylian Mbappe'),
('12','Matthijs de Ligt'),
('13','Robert Lewandowski'),
('14','Papu Gomez'),
('15','Joshua Kimmich'),
('16','Sergio Aguero'),
('17','Dusan Tadic'),
('18','Jan Oblak'),
('19','Kalidou Koulibaly'),
('20','Trent Alexander-Arnold'),
('21','Andy Robertson'),
('22','Marc-Andre Ter Stegen'),
('23','Fabio Quagliarella'),
('24','Miralem Pjanic'),
('25','Christian Eriksen'),
('26','Jadon Sancho'),
('27','Pierre-Emerick Aubameyang'),
('28','Diego Godin'),
('29','Duvan Zapata'),
('30','Harry Kane'),
('31','Son Heung-min'),
('32','Giorgio Chiellini'),
('33','Rodri'),
('34','Luka Modric'),
('35','Jordi Alba'),
('36','Samir Handanovic'),
('37','Milan Skriniar'),
('38','Hakim Ziyech'),
('39','Luka Jovic'),
('40','Krzysztof Piatek'),
('41','Antoine Griezmann'),
('42','Marco Reus'),
('43','Kai Havertz'),
('44','Tanguy Ndombele'),
('45','Gerard Pique'),
('46','Joao Cancelo'),
('47','Neymar'),
('48','Nicolas Pepe'),
('49','Ederson Moraes'),
('50','Toni Kroos'),
('51','Thiago Alcantara'),
('52','Josip Ilicic'),
('53','Aymeric Laporte'),
('54','Sergio Ramos'),
('55','Angel di Maria'),
('56','Paul Pogba'),
('57','Fernandinho'),
('58','N`Golo Kante'),
('59','Marquinhos'),
('60','Mousa Sissoko'),
('61','Donny van de Beek'),
('62','Dani Parejo'),
('63','Fabian Ruiz'),
('64','Wili Orban'),
('65','Karim Benzema'),
('66','Ibrahima Konate'),
('67','Alexandre Lacazette'),
('68','Edinson Cavani'),
('69','Sergej Milinkovic-Savic'),
('70','Marco Verratti'),
('71','Pablo Sarabia'),
('72','Alessio Romagnoli'),
('73','Axel Witsel'),
('74','Allan'),
('75','Serge Gnabry'),
('76','Fabinho'),
('77','Jan Vertonghen'),
('78','Wojciech Szczesny'),
('79','Timo Werner'),
('80','Arthur'),
('81','Lorenzo Insigne'),
('82','Roberto Firmino'),
('83','Iago Aspas'),
('84','David Neres'),
('85','Sebastian Haller'),
('86','Arkadiusz Milik'),
('87','Gianluigi Donnarumma'),
('88','Giovani Lo Celso'),
('89','Giorginio Wijnaldum'),
('90','Luis Suarez'),
('91','Nicolo Barella'),
('92','Aaron Wan-Bissaka'),
('93','Marcelo Brozovic'),
('94','Florian Thauvin'),
('95','Kepa Arrizbalaga'),
('96','Paco Alcacer'),
('97','Jordan Henderson'),
('98','Ferland Mendy'),
('99','Hans Hateboer'),
('100','Thorgan Hazard');
