
DROP TABLE IF EXISTS products;
CREATE TABLE products(
  barcode varchar(50) NOT NULL,
  barangShort varchar(15) NOT NULL,
  barangLong  tinytext,
  satuan varchar(3) NOT NULL,
  -- harga0 == harga pokok jual
  harga0 int(6) DEFAULT 0,
  harga1 int(6) DEFAULT 0,
  harga2 int(6) DEFAULT 0,
  eceran int(6) DEFAULT 0,
  diskon float(5,2) DEFAULT 0.00,
  PRIMARY KEY(barcode)
);

DROP TABLE IF EXISTS customers;
CREATE TABLE customers(
  customerId int(4) UNSIGNED ZEROFILL AUTO_INCREMENT,
  nama varchar(30) NOT NULL,
  PRIMARY KEY(customerId)
);

DROP TABLE IF EXISTS invoiceInfo;
CREATE TABLE invoiceInfo(
  -- trxNumber Format: YYMMXXXX
  trxNumber int(8) NOT NULL,
  tanggal date,
  customerId int(4),
  PRIMARY KEY(trxNumber)
);

DROP TABLE IF EXISTS invoiceData;
CREATE TABLE invoiceData(
  trxNumber int(8) UNSIGNED ZEROFILL,
  trxIndex int(8) UNSIGNED ZEROFILL AUTO_INCREMENT,
  barcode varchar(50) NOT NULL,
  quantity int(2) DEFAULT 1,
  givenPrice int(6) DEFAULT 0,
  givenDisc  int(6) DEFAULT 0,
  PRIMARY KEY(trxIndex)
);

DROP TABLE IF EXISTS trxEceran;
CREATE TABLE trxEceran(
  ecerIdx int(8) UNSIGNED ZEROFILL AUTO_INCREMENT,
  tanggal date,
  barcode varchar(50) NOT NULL,
  quantity int(2) DEFAULT 1,
  ecerPrice int(6) DEFAULT 0,
  PRIMARY KEY(ecerIdx)
);