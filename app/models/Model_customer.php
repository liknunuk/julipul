<?php
class Model_customer
{
    private $table = "customers";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function daftarCustomer($hal){
        $row = ( $hal - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY nama LIMIT $row,48";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function dataCustomer($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE customerId = :id LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    public function tambahCustomer($data){
        $sql = "INSERT INTO " . $this->table . " SET nama = :nama ";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateCustomer($data){
        $sql = "UPDATE " . $this->table . " SET nama = :nama WHERE customerId = :customerId";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('customerId',$data['customerId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function pecatCustomer($id){
        $sql = "DELETE FROM " . $this->table . " WHERE customerId = :id LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

}
