<?php
class Model_produk
{
    private $table = "products";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function daftarProduk($hal){
        $row = ( $hal - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY barangLong LIMIT " . $row . "," .rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function tambahProduk($data){
        // barcode=:barcode, barangLong=:barangLong, barangShort=:barangShort, satuan=:satuan, harga0=:harga0, harga1=:harga1, harga2=:harga2, eceran=:eceran, diskon=:diskon, 
        $sql = "INSERT INTO " . $this->table . " SET barcode=:barcode, barangLong=:barangLong, barangShort=:barangShort, satuan=:satuan, harga0=:harga0, harga1=:harga1, harga2=:harga2, eceran=:eceran, diskon=:diskon ";
        $this->db->query($sql);
        $this->db->bind('barcode',$data['barcode']);
        $this->db->bind('barangLong',$data['barangLong']);
        $this->db->bind('barangShort',$data['barangShort']);
        $this->db->bind('satuan',$data['satuan']);
        $this->db->bind('harga0',$data['harga0']);
        $this->db->bind('harga1',$data['harga1']);
        $this->db->bind('harga2',$data['harga2']);
        $this->db->bind('eceran',$data['eceran']);
        $this->db->bind('diskon',$data['diskon']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateProduk($data){
        // barcode=:barcode, barangLong=:barangLong, barangShort=:barangShort, satuan=:satuan, harga0=:harga0, harga1=:harga1, harga2=:harga2, eceran=:eceran, diskon=:diskon, 
        $sql = "UPDATE " . $this->table . " SET barangLong=:barangLong, barangShort=:barangShort, satuan=:satuan, harga0=:harga0, harga1=:harga1, harga2=:harga2, eceran=:eceran, diskon=:diskon WHERE barcode=:barcode LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('barcode',$data['barcode']);
        $this->db->bind('barangLong',$data['barangLong']);
        $this->db->bind('barangShort',$data['barangShort']);
        $this->db->bind('satuan',$data['satuan']);
        $this->db->bind('harga0',$data['harga0']);
        $this->db->bind('harga1',$data['harga1']);
        $this->db->bind('harga2',$data['harga2']);
        $this->db->bind('eceran',$data['eceran']);
        $this->db->bind('diskon',$data['diskon']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dataProduk($barcode){
        $sql = "SELECT * FROM " . $this->table . " WHERE barcode = :barcode LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('barcode',$barcode);
        return $this->db->resultOne();
    }

    public function hapusProduk($barcode){
        $sql = "DELETE FROM " . $this->table . " WHERE barcode = :barcode LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('barcode',$barcode);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function cariProduk($nama){
        $nama = "%".$nama."%";
        $nama = str_replace("-"," ",$nama);
        $sql = "SELECT * FROM " . $this->table . " WHERE barangLong LIKE :nama ORDER BY barangLong LIMIT ". rows;
        $this->db->query($sql);
        $this->db->bind('nama',$nama);
        return $this->db->resultSet();
    }

}
