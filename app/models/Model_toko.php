<?php
class Model_toko
{
    private $tbProduk = "products";
    private $tbCustmr = "customers";
    private $tbTxInfo = "invoiceInfo";
    private $tbTxData = "invoiceData";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function newBakulanId(){
        $tgl = date('ym')."%";
        $sql = "SELECT MAX(trxNumber) trxNumber FROM " . $this->tbTxInfo . " WHERE trxNumber LIKE :tgl";
        $this->db->query($sql);
        $this->db->bind('tgl',$tgl);
        $result = $this->db->resultOne();
        $txNumber = $result['trxNumber'] == NULL ? date('ym').'0001' : $result['trxNumber'] + 1;
        return $txNumber;
    }

    public function setNewBakulan($data){
        $sql = "INSERT INTO " . $this->tbTxInfo . " SET trxNumber = :trxNumber , customerId = :customerId , tanggal = :tanggal ";
        $this->db->query($sql);
        $this->db->bind('trxNumber',$data['trxNumber']);
        $this->db->bind('customerId',$data['customerId']);
        $this->db->bind('tanggal',date('Y-m-d'));
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function setBakulan($data){
        $sql = " INSERT INTO " . $this->tbTxData . " SET trxNumber=:trxNumber,barcode=:barcode,quantity=:quantity,givenPrice=:givenPrice,givenDisc=(SELECT diskon FROM products WHERE barcode = :barcode) ";
        $this->db->query($sql);
        $this->db->bind('trxNumber',$data['trxNumber']);
        $this->db->bind('barcode',$data['barcode']);
        $this->db->bind('quantity',$data['quantity']);
        $this->db->bind('givenPrice',$data['givenPrice']);
        // $this->db->bind('givenDisc',$data['givenDisc']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function customers($nama){
        $sql = "SELECT * FROM ". $this->tbCustmr . " WHERE nama LIKE :nama ORDER BY nama LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('nama','%'.$nama.'%');
        return $this->db->resultSet();
    }

    public function hargaBakulan($barcode){
        $sql = "SELECT harga1, harga2, diskon FROM " .$this->tbProduk . " WHERE barcode = :barcode LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('barcode',$barcode);
        return $this->db->resultOne();
    }

    public function newCustId(){
        $sql = "SELECT MAX(customerId) custId FROM " . $this->tbCustmr;
        $this->db->query($sql);
        $result = $this->db->resultOne();
        $custId = $result['custId'] == NULL ? '0001' : $result['custId'] + 1;
        return $custId;
    }

    public function showBeceran($trxId){
        // trxNumber,trxIndex,barcode,quantity,givenPrice,givenDisc
        $sql = "SELECT invoiceData.*, ( (1 - ( invoiceData.givenDisc / 100 )) * invoiceData.quantity * invoiceData.givenPrice ) AS subTotal , products.barangLong FROM invoiceData,products WHERE trxNumber = :trxId && products.barcode = invoiceData.barcode ORDER BY trxIndex DESC";

        $this->db->query($sql);
        $this->db->bind('trxId',$trxId);
        return $this->db->resultSet();
    }

    public function struk($trxId){
        $sql = "SELECT * FROM invoiceInfo, customers WHERE trxNumber = :trxId && customers.customerId = invoiceInfo.customerId";
        $this->db->query($sql);
        $this->db->bind('trxId',$trxId);
        $info = $this->db->resultOne();
    
        $sql = "SELECT invoiceData.*, products.barangShort barang, products.barangLong produk, products.satuan FROM invoiceData,products WHERE trxNumber = :trxId && products.barcode = invoiceData.barcode ORDER BY trxIndex";
        $this->db->query($sql);
        $this->db->bind('trxId',$trxId);
        $data = $this->db->resultSet();
    
        return array('info'=>$info,'data'=>$data);
      }

      public function arsipNota($tgl){
          $sql = "SELECT invoiceInfo.*, customers.nama, SUM(quantity) quantity, SUM(givenPrice * quantity * (1-(givenDisc/100))) subTotal FROM invoiceInfo, customers, invoiceData WHERE invoiceInfo.tanggal=:tgl && customers.customerId = invoiceInfo.customerId && invoiceData.trxNumber = invoiceInfo.trxNumber GROUP BY invoiceData.trxNumber ORDER BY invoiceInfo.trxNumber DESC";

          $this->db->query($sql);
          $this->db->bind('tgl',$tgl);
          return $this->db->resultSet();
      }

}
