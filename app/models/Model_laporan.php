<?php
Class Model_laporan
{
    private $table = "vTrx";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function lapTransaksi($waktu="",$hal=1){
        $row = ($hal-1) * rows;
        $waktu = $waktu == "" ? date('Y-m-d') : $waktu;
        $sql = " SELECT trxNumber,barangLong,quantity,satuan,hargaPokok,givenPrice,givenDisc,hargaPokok,laba akLaba FROM vTrx WHERE tanggal LIKE :waktu LIMIT $row," . rows;
        $this->db->query($sql);
        $this->db->bind('waktu',$waktu.'%');
        return $this->db->resultSet();
    }

    public function lapBarang($waktu="",$hal=1){
        $row = ($hal - 1) * rows;
        $waktu = $waktu == "" ? date('Y-m-d') : $waktu;

        $sql = "SELECT barangLong , SUM(quantity) quantity , SUM(hargaPokok * quantity) modal, SUM(givenPrice * quantity) totalJual,SUM((givenDisc/100) * givenPrice * quantity) totalDiskon, SUM(laba) akLaba FROM vTrx WHERE tanggal LIKE :waktu GROUP BY barcode ORDER BY barangLong LIMIT $row,".rows;

        $this->db->query($sql);
        $this->db->bind('waktu',$waktu.'%');
        return $this->db->resultSet();
    }

    public function lapPelanggan($waktu="",$hal=1){
        $row = ($hal - 1) * rows;
        $waktu = $waktu == "" ? date('Y-m-d') : $waktu;

        $sql = "SELECT customerId , nama , SUM(quantity) totalItem, SUM(givenPrice) totalPrice , SUM(givenDisc/100 * quantity * givenPrice) totalDiskon, SUM(laba) totalLaba FROM vTrx WHERE tanggal LIKE :waktu GROUP BY customerId ORDER BY nama LIMIT $row,".rows;

        $this->db->query($sql);
        $this->db->bind('waktu',$waktu.'%');
        return $this->db->resultSet();
    }

    public function resumeLaba($bulan=""){
        $bulan = $bulan == "" ? date('Y-m')."%" : $bulan."%";
        $sql = "SELECT DATE_FORMAT(tanggal,'%d/%m/%y') tanggal, SUM(quantity * hargaPokok) as modal , SUM((quantity * (100-givenDisc)/100) * givenPrice ) as omset, SUM(laba) as akumulasiLaba FROM vTrx WHERE tanggal LIKE :bulan GROUP BY tanggal ORDER BY tanggal";
    
        $this->db->query($sql);
        $this->db->bind('bulan',$bulan);
        return $this->db->resultSet();
    }

}