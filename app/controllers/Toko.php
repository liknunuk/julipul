<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Toko extends Controller
{
  
  public function index()
  {
    $data['title'] = "Toko";
    $this->view('template/header',$data);
    $this->view('toko/index',$data);
    $this->view('template/footer');
  }
  
  public function info($data,$info=""){
      if($data == 'newTrxId'){
        echo $this->model('Model_toko')->newBakulanId();
      }

      if($data == 'customers'){
          $customers = $this->model('Model_toko')->customers($info);
          echo json_encode($customers,JSON_PRETTY_PRINT);
      }

      if($data == 'bakulanPrice'){
        $prices = $this->model('Model_toko')->hargaBakulan($info);
        echo json_encode($prices,JSON_PRETTY_PRINT);
      }

      if($data == 'newCustId'){
        $customerId = $this->model('Model_toko')->newCustId();
        echo sprintf('%04d',$customerId);
      }

      if($data == 'beceran' ){
        $beceran = $this->model('Model_toko')->showBeceran($info);
        echo json_encode($beceran,JSON_PRETTY_PRINT);
      }
  }

  public function newInvoice(){
    echo $this->model('Model_toko')->setNewBakulan($_POST);
    // print_r($_POST);
  }

  public function setBakulan(){
    if ($this->model('Model_toko')->setBakulan($_POST) > 0){
      echo "1";
    }
  }

  

  public function newCustomer(){
    if ($this->model('Model_customer')->tambahCustomer($_POST) > 0 ){
      $newTrxId = $this->model('Model_toko')->newBakulanId();
      $resp = [ 'customerId'=> $_POST['customerId'],'trxNumber' => $newTrxId ];
      echo json_encode($resp,JSON_PRETTY_PRINT);
    }
  }

  public function nota($trxId){
    $data['nota'] = $this->model('Model_toko')->struk($trxId);
    $this->view('toko/nota',$data);
  }

  public function nota2($trxId){
    $data['nota'] = $this->model('Model_toko')->struk($trxId);
    $this->view('template/cors');
    echo json_encode($data['nota']);
  }

  public function arsip($tgl=""){
    if($tgl == "") { $tgl = date('Y-m-d'); }
    $data['title']="Arsip Nota";
    $data['nota'] = $this->model('Model_toko')->arsipNota($tgl);

    $this->view('template/header',$data);
    $this->view('template/header-toko',$data);
    $this->view('toko/arsipnota',$data);
    $this->view('template/footer');
  }

  public function rincian($trxId){
    $data['title'] = 'Rincian Belanja';
    $data['nota'] = $this->model('Model_toko')->struk($trxId);

    $this->view('template/header',$data);
    $this->view('template/header-toko',$data);
    $this->view('toko/rincinota',$data);
    $this->view('template/footer');
  }
}
