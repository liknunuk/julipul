<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Manager extends Controller
{
  // method default
  public function index($pageNumber = 1)
  {
    $data['title'] = "Management Toko";
    $data['hal'] = $pageNumber;
    $data['produk'] = $this->model('Model_produk')->daftarProduk($pageNumber);

    $this->view('template/header',$data);
    $this->view('template/navbar',$data);
    $this->view('manager/products',$data);
    $this->view('template/header');
  }

  public function produk($pageNumber = 1){
      $data['title']='DAFTAR BARANG PENJUALAN';
      $data['hal'] = $pageNumber;
      $data['produk'] = $this->model('Model_produk')->daftarProduk($pageNumber);
      $this->view('template/header',$data);
      $this->view('template/navbar',$data);
      $this->view('manager/products',$data);
      $this->view('template/footer');
  }

  public function formProduk($mod,$barcode=""){
      $data['title']='DAFTAR BARANG PENJUALAN';
      $data['mod'] = $mod;
      if( $mod == 'chg' && $barcode != "" ){
          $data['produk']=$this->model('Model_produk')->dataProduk($barcode);
      }else{
          $data['produk'] = ['barcode'=>'','barangShort'=>'','barangLong'=>'','satuan'=>'','harga0'=>'','harga1'=>'','harga2'=>'','eceran'=>'','diskon'=>''];
      }
      $this->view('template/header',$data);
      $this->view('template/navbar',$data);
      $this->view('manager/formProduk',$data);
      $this->view('template/footer');
  }

  public function setProduk($mod){
      if($mod == 'new'){
        if( $this->model('Model_produk')->tambahProduk($_POST) > 0 ){
            Alert::setAlert('Berhasil Ditambahkan','Data Produk','success');
        }
      }elseif($mod == 'chg'){
        if( $this->model('Model_produk')->updateProduk($_POST) > 0 ){
            Alert::setAlert('Berhasil Dimutakhirkan','Data Produk','success');
        }
      }else{
          echo "";
      }
      header("Location:" . BASEURL . "manager/produk");
  }

  public function hapusProduk(){
    //   print_r($_POST);
    $response =  $this->model('Model_produk')->hapusProduk($_POST['barcode']);
    print($response);
  }

  public function cariProduk($barangLong){
    $products = $this->model('Model_produk')->cariProduk($barangLong);
    echo json_encode($products,JSON_PRETTY_PRINT);
  }

  // Segmen Customer
  public function customers($hal=1,$mod="new",$id=""){
    $data['title'] = "Pelanggan";
    $data['mod'] = $mod;
    $data['hal'] = $hal;
    $data['customers'] = $this->model('Model_customer')->daftarCustomer($hal);
    
    if( $mod == "chg" && $id != ""){
      $data['customer'] = $this->model('Model_customer')->dataCustomer($id);
    }else{
      $data['customer']=['customerId'=>'','nama'=>''];
    }

    $this->view('template/header',$data);
    $this->view('template/navbar',$data);
    $this->view('manager/customers',$data);
    $this->view('template/footer');
  }

  

  public function setCustomer($mod){
    if($mod == 'new' ){
      if($this->model('Model_customer')->tambahCustomer($_POST) > 0 ){
        Alert::setAlert("berhasil ditambahkan","Data Pelanggan","success");
      }
    }elseif($mod=="chg"){
      if($this->model('Model_customer')->updateCustomer($_POST) > 0 ){
        Alert::setAlert("berhasil diubah","Data Pelanggan","success");
      }
    }else{

    }

    header("Location:".BASEURL."Manager/customers");
  }

  public function pecatCustomer(){
    // print_r($_POST);
    $row = $this->model('Model_customer')->pecatCustomer($_POST['customerId']);
    echo $row;
  }

  // Segmen Laporan

  public function laporan(){
    $data['title'] = "Laporan";

    $this->view('template/header',$data);
    $this->view('template/navbar');
    echo "<h2>Laporan</a>";
    $this->view('template/footer');
  }

  public function lapoTrx($waktu ="",$hal=1){

    $data['title'] = "Laporan Penjualan";
    $data['trx'] = $this->model('Model_laporan')->lapTransaksi($waktu,$hal);
    $data['hal'] = $hal;
    $data['tgl'] = $waktu == "" ? date('Y-m-d') : $waktu;

    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('manager/lapTransaksi',$data);
    $this->view('template/footer');
  }

  public function lapoBar($waktu="",$hal=1){
    $data['title'] = "Laporan Penjualan Barang";
    $data['trx'] = $this->model('Model_laporan')->lapBarang($waktu,$hal);
    $data['hal'] = $hal;
    $data['tgl'] = $waktu == "" ? date('Y-m-d') : $waktu;

    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('manager/lapTrxBarang',$data);
    $this->view('template/footer');

  }

  public function lapoGan($waktu="",$hal=1){
    $data['title'] = "Transaksi Pelanggan";
    $data['trx'] = $this->model('Model_laporan')->lapPelanggan($waktu,$hal);
    $data['hal'] = $hal;
    $data['tgl'] = $waktu == "" ? date('Y-m-d') : $waktu;

    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('manager/lapTrxCustmr',$data);
    $this->view('template/footer');
  }

  public function ikhtisar($bulan=""){
    $bulan = $bulan == "" ? date('Y-m') : $bulan;

    $data['title'] = 'Ringkasan Laba';
    $data['ikhtisar'] = $this->model('Model_laporan')->resumeLaba($bulan);
    $data['bulan'] = $bulan;

    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('manager/lapIkhtisar',$data);
    $this->view('template/footer');

  }
}
