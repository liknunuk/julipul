<?php

list($t,$b,$h) = explode('-',$data['nota']['info']['tanggal']);

?>
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-3">
            <table class="table table-sm">
                <tbody>
                    <tr>
                        <th>Nomor Nota</th>
                    </tr>
                    <tr>
                        <td><?=$data['nota']['info']['trxNumber'];?></td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                    </tr>
                    <tr>
                        <td><?="$h-$b-$t";?></td>
                    </tr>
                    <tr>
                        <th>Pelanggan</th>
                    </tr>
                    <tr>
                        <td><?=sprintf('%04d',$data['nota']['info']['customerId']) .' - '.$data['nota']['info']['nama'];?></td>
                    </tr>
                    <tr>
                        <td>
                        <a href="javascript:void(0)" onclick ="window.open('<?=BASEURL;?>Toko/nota/<?=$data['nota']['info']['trxNumber'];?>','nota','width=400,height=800,left=400')">Cetak</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-9">
            <table class="table table-striped table-bordered">
                <thead class='bg-success'>
                    <tr class='text-center'>
                        <th>Nama Barang</th>
                        <th>Quantiy</th>
                        <th>Satuan</th>
                        <th>Harga Satuan</th>
                        <th>Jumlah Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $totalDiskon = 0;
                        $totalPrice  = 0;
                        $totalItem   = 0;
                        foreach($data['nota']['data'] AS $barang):

                        $subTotal = $barang['quantity'] * $barang['givenPrice'];
                        $barang['quantity'].' '.$barang['satuan'].' '.$barang['barang'];
                        $subDiskon = $barang['givenDisc'] / 100 * $barang['quantity'] * $barang['givenPrice'];
                        $totalDiskon += $subDiskon;
                        $totalPrice  += $subTotal;
                        $totalItem   += $barang['quantity'];
                    ?>
                    <tr>
                        <td><?=$barang['produk'];?></td>
                        <td class='text-right'><?=$barang['quantity'];?></td>
                        <td><?=$barang['satuan'];?></td>
                        <td class="text-right"><?=number_format($barang['givenPrice'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($subTotal,0,',','.');?></td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>Jumlah</td>
                        <td class='text-right'><?=$totalItem;?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class='text-right'><?=number_format($totalPrice,0,',','.');?></td>
                    </tr>
                    <tr>
                        <td>Diskon</td>
                        <td colspan="3">&nbsp;</td>
                        <td class='text-right'><?=number_format($totalDiskon,0,',','.');?></td>
                    </tr>
                    <tr>
                        <td>Penerimaan</td>
                        <td colspan="3">&nbsp;</td>
                        <td class='text-right'>
                            <?php
                            $tagihan = $totalPrice - $totalDiskon;
                            echo number_format($tagihan,0,',','.');
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
                        
            <span class="float-right">
                <a href="<?=BASEURL;?>Toko/arsip/<?="{$t}-{$b}-{$h}";?>">Kembali</a>
            </span>
        </div>
    </div>
</div>
