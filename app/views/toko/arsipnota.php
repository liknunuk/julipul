<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Arsip Nota</h2>
            <div class="form-group">
                <span>
                Tanggal: <input type="date" id="tglNota" value="<?=date('Y-m-d');?>">
                <a href="javascript:void(0)" id="cekNota">Cek</a>
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12"></div>
    </div>

    <div class="row mt-3">
        <div class="col-lg-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Jumlah Item</th>
                        <th>Jumlah Belanja</th>
                        <th>Detil</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['nota'] as $nota): ?>
                    <tr>
                        <td><?=$nota['trxNumber'];?></td>
                        <td><?=$nota['tanggal'];?></td>
                        <td><?=$nota['nama'];?></td>
                        <td class="text-right"><?=$nota['quantity'];?></td>
                        <td class="text-right"><?=number_format($nota['subTotal'],0,',','.');?></td>
                        <td>
                            <a href="<?=BASEURL;?>Toko/rincian/<?=$nota['trxNumber']?>">Detil</a>
                            |
                            <a href="#" onclick ="window.open('<?=BASEURL;?>Toko/nota/<?=$nota['trxNumber'];?>','nota','width=400,height=800,left=400')">Cetak</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <span class="float-right">
                <a href="<?=BASEURL;?>Toko">Kembali</a>
            </span>
        </div>
    </div>

</div>
<?php $this->view('template/bs4js'); ?>
<script>
$('#cekNota').click( function(){
    let tanggal = $("#tglNota").val(),
        url = "<?=BASEURL;?>Toko/arsip/"+tanggal;
    location.href=url;
})
</script>