<?php

list($t,$b,$h) = explode('-',$data['nota']['info']['tanggal']);

?>
<table class='nota' style="font-family:monospace; font-size: 10pt;">
    <tbody>
        <tr>
            <td>Nomor Nota</td>
            <td><?=$data['nota']['info']['trxNumber'];?></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td><?="$h-$b-$t";?></td>
        </tr>
        <tr>
            <td>Pelanggan</td>
            <td><?=sprintf('%04d',$data['nota']['info']['customerId']) .' '.$data['nota']['info']['nama'];?></td>
        </tr>
    </tbody>
</table>
<br>
<table class='nota' style="font-family:monospace; font-size: 10pt; width:100%;" cellspacing=0>
    <tbody>
        
        <?php 
            $totalDiskon = 0;
            $totalPrice  = 0;
            foreach($data['nota']['data'] AS $barang): ?>
        <?php   
            $subTotal = $barang['quantity'] * $barang['givenPrice'];
            $barang['quantity'].' '.$barang['satuan'].' '.$barang['barang'];
            $subDiskon = $barang['givenDisc'] / 100 * $barang['quantity'] * $barang['givenPrice'];
            $totalDiskon += $subDiskon;
            $totalPrice  += $subTotal;
        ?>
        <tr>
            <td>
                <?=$barang['quantity'].' '.$barang['satuan'].' '.$barang['barang'];?>
            </td>
            <td align='right'><?=number_format($barang['givenPrice'],0,',','.');?></td>
            <td align='right'><?=number_format($subTotal,0,',','.');?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan='2' style="border-top:1px solid black;">Total</td>
            <td align='right' style="border-top:1px solid black;"><?=number_format($totalPrice,0,',','.');?></td>
        </tr>
        <tr>
            <td colspan='2'>Diskon</td>
            <td align='right'><?=number_format($totalDiskon,0,',','.');?></td>
        </tr>
        <tr>
            <td colspan='2'>Tagihan</td>
            <td align='right'>
                <?php
                    $tagihan = $totalPrice - $totalDiskon;
                    echo number_format($tagihan,0,',','.');
                ?>
            </td>
        </tr>
    </tbody>
</table>
<br>
<a href="<?=BASEURL;?>Toko" style="font-family:monospace;">Terima Kasih</a>

<?php
$filoc = dirname(__FILE__,4);
exec($filoc.'/nota.py '.$data['nota']['info']['trxNumber'] . '>> /dev/usb/lp0');
?>