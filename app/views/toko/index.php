<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 banner">
            <div class="ban1">Toko Saepul</div>
            <div class="ban2">Jalan Selamanik</div>
            <div class="ban3">Telp: ......</div>
        </div>
    </div>
    <!-- end of shop banner  -->
    <div class="row mt-3 list-inline">
        <li class="list-inline-item btn btn-info" id="trxBakulan">Bakulan</li>
        <!-- <li class="list-inline-item btn btn-info" id="trxEceran">Eceran</li> -->
        <li class="list-inline-item btn btn-info" id="trxArchive">Arsip</li>
        <li class="list-inline-item btn btn-info" id="trxPrint">Cetak</li>
    </div>
        
    <div class="row mt-3">
        <div class="col-lg-4 form-horizontal">
            
            <div class="form-group row fg-tipis">
                <label class="col-sm-3">No. Trx</label>
                <div class="col-sm-9">
                    <input type="text" id="trxId" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row fg-tipis">
                <label class="col-sm-3">Tanggal</label>
                <div class="col-sm-9">
                    <input type="text" id="trxDate" class="form-control" readonly>
                </div>
            </div>

        </div>
        <div class="col-lg-4 form-horizontal">

            <div class="form-group row fg-tipis">
                <label class="col-sm-3">Pelanggan</label>
                <div class="col-sm-9">
                    <input type="text" id="trxCust" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row fg-tipis">
                <label class="col-sm-3">Barcode</label>
                <div class="col-sm-9">
                    <input type="number" id="trxBarcode" class="form-control" placeHolder="Scan Barcode">
                </div>
            </div>

        </div>

        <div class="col-lg-4 form-horizontal">

            <div class="form-group row fg-tipis">
                <label class="col-sm-3">Banyaknya</label>
                <div class="col-sm-9">
                    <input type="number" id="trxQty" class="form-control text-right" value="1" min="1">
                </div>
            </div>

            <div class="form-group row fg-tipis">
                <label class="col-sm-3">Harga</label>
                <div class="col-sm-9">
                    <select id="trxHarga" class="form-control text-right"></select>
                </div>
            </div>

            <input type="hidden" id="trxDiskon">

        </div>
    </div>
    <!-- end of data transaksi -->
    <div class="row mt-3">
        <div class="col-lg-12 table-responsive">
            <table class="table table-sm table-bordered">
                <thead class="bg-success">
                    <tr class="text-center">
                        <th width="150">Kode Pembelian</th>
                        <th>Nama Barang</th>
                        <th>Banyaknya</th>
                        <th>Harga</th>
                        <th>Diskon</th>
                        <th>Jumlah Harga</th>
                    </tr>
                </thead>
                <tbody id="beceran"></tbody>
            </table>
        </div>
    </div>
    <div class="row"></div>
</div>
<!-- Modal Bakulan -->
<div class="modal" tabindex="-1" role="dialog" id="mdBakulan">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Transaksi Bakulan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">

            <div class="form-group row">
                <label class="col-sm-4">Nomor Trx</label>
                <div class="col-sm-8">
                    <input type="number" id="mdTrxId" class="form-control form-control-sm">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-4">Nama Pelanggan</label>
                <div class="col-sm-8">
                    <input type="text" id="mdCust" class="form-control form-control-sm" placeholder="Cari Pelanggan" list="customerList">
                    <datalist id="customerList"></datalist>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-4">Pelanggan Baru</label>
                <div class="col-sm-8">
                    <input type="text" id="mdNewCust" class="form-control form-control-sm" placeholder="Tulis Nama Pelanggan">
                </div>
            </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="mdSetBakulan" class="btn btn-primary">Mulai</button>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $(document).ready( function(){
        checkBarcode();
        let trxid = $("#trxId").val();
        if( trxid ==''){ 
            $("#trxPrint").hide(); 
        }else{
            $("#trxPrint").show();
        }
    })

    $('#trxBakulan').click( function(){
        $('#mdBakulan').modal('toggle');
        $.ajax({
            url: "<?=BASEURL;?>Toko/info/newTrxId",
            success: function(resp){
                $("#mdTrxId").val(resp);
            }
        })
        $('#mdCust').focus();
    })

    $("#mdCust").on('keypress',function(event){
        let cust = $(this).val();
        // event.preventDefault();
        if( event.keyCode == 13 && cust.length > 0 ){
            event.preventDefault();
            let trxNumber = $('#mdTrxId').val(),
                customerId = $('#mdCust').val().split(':');
            //  console.log( trxNumber +','+ customerId[0]);
            $("#trxId").val(trxNumber);
            $("#trxDate").val("<?=date('Y-m-d');?>");
            $("#trxCust").val( customerId[0]);
            $("#trxPrint").show();
            $.post(
                `<?=BASEURL;?>Toko/newInvoice`,{
                    trxNumber : trxNumber,
                    customerId: customerId[0]
                },function(resp){
                    // console.log(resp);
                    if( resp == '1' ){
                        $('#mdBakulan').modal('toggle');
                        $('#mdTrxId').val('');
                        $('#mdCust').val('');
                        $('#trxBarcode').focus();
                    }
                }
            );
        }else if(event.keyCode == 13 && cust.length == 0){
            $.ajax({
                url: '<?=BASEURL;?>Toko/info/newCustId',
                success: function(resp){
                    $("#mdNewCust").val(resp+':');
                }
            })
            $('#mdNewCust').focus();
        }else{
            // event.preventDefault();
            customerCheck($(this).val());
        }
    })

    $("#mdNewCust").on('keypress', function(event){
        if( event.keyCode == 13 ){
            event.preventDefault();
            cdata = $(this).val().split(':');
            $.post('<?=BASEURL;?>Toko/newCustomer',{
                customerId: cdata[0],
                nama : cdata[1]
            },function(resp){
                let result = JSON.parse(resp);
                // console.log(result);
                $("#trxId").val(result.trxNumber);
                $("#trxDate").val("<?=date('Y-m-d');?>");
                $("#trxCust").val( result.customerId );

                $('#mdBakulan').modal('toggle');
                $('#mdTrxId').val('');
                $('#mdCust').val('');
                $('#trxBarcode').focus();
                $('#trsPrint').show();
            })
        }
    })

    $('#trxBarcode').blur( function(){
        $.ajax({
            dataType: 'json',
            url: '<?=BASEURL;?>Toko/info/bakulanPrice/'+$("#trxBarcode").val(),
            success: function(resp){
                $("#trxHarga option").remove();
                $("#trxHarga").append(`
                <option>${resp.harga1}</option>
                <option>${resp.harga2}</option>
                `);

                $('#trxDiskon').val(resp.diskon);
            }
        })
    })

    $('#trxQty').on('keypress',function(event){
        if( event.keyCode == 13 ){
            event.preventDefault();
            $("#trxHarga").focus();
        }
    })

    $('#trxHarga').on('keypress',function(event){
        if( event.keyCode == 13 ){
            event.preventDefault();
            let trxNumber, barcode, quantity, givenPrice;
            trxNumber   = $("#trxId").val();
            barcode     = $("#trxBarcode").val();
            quantity    = $("#trxQty").val();
            givenPrice  = $("#trxHarga").val();
            diskon      = $("#trxDiskon").val();
            $.post(
                `<?=BASEURL;?>Toko/setBakulan`,{
                    trxNumber   : trxNumber,
                    barcode     : barcode,
                    quantity    : quantity,
                    givenPrice  : givenPrice,
                    diskon      : diskon
                },function(resp){
                    // console.log(resp);
                    if( resp == '1' ){
                        $("#trxBarcode").val('');
                        $("#trxBarcode").focus();
                        $("#trxHarga").val('');
                        $("#trxQty").val(1);
                        showBelanjaan(trxNumber);
                    }

                }
            );
        }
    })

    $("#trxArchive").click( function(){
        window.location.href = "<?=BASEURL;?>Toko/arsip";
    })

    $("#trxPrint").click(function(){
        // alert('Print');
        let trxId = $('#trxId').val();
        window.location='<?=BASEURL;?>Toko/nota/'+trxId;
    })

    function checkBarcode(){
        setInterval(function(){ 
            let barcode = $("#trxBarcode").val();
            if(barcode.length > 0 && $("#trxBarcode").is(':focus')  ){
                $("#trxQty").focus();
            }
        }, 1000);
    }

    function showBelanjaan(trxId){
        let itemQty=0,totalPrice=0;
        $.ajax({
            dataType:'json',
            url: '<?=BASEURL;?>Toko/info/beceran/'+trxId,
            success: function(resp){
                $('#beceran tr').remove();
                $.each(resp, function(i,data){
                    let diskone = parseInt(data.givenDisc) * parseInt(data.givenPrice) / 100;
                    $("#beceran").append(`
                    <tr>
                    <td>${data.trxIndex}</td>
                    <td>${data.barangLong}</td>
                    <td class='text-right'>${data.quantity}</td>
                    <td class='text-right'>${new Intl.NumberFormat('id-ID').format(data.givenPrice)}</td>
                    <td class='text-right'>${new Intl.NumberFormat('id-ID').format(diskone)}</td>
                    <td class='text-right'>${new Intl.NumberFormat('id-ID').format(data.subTotal)}</td>
                    </tr>
                    `)
                    itemQty += parseInt(data.quantity);
                    totalPrice += parseInt(data.subTotal);
                })
                $('#beceran').append(`
                    <tr class='bg-secondary'>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class='text-right'>${itemQty}</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class='text-right'>${new Intl.NumberFormat('id-ID').format(totalPrice)}</td>
                    </tr>
                `)
            }
        })
    }

    function customerCheck(nama){
        // $("#mdCust").keyup( function(){
            // let custName = $(this).val();
            if( nama.length >= 2 ){
                $('#customerList option').remove();
                $.ajax({
                    dataType:'json',
                    url:'<?=BASEURL;?>Toko/info/customers/'+nama,
                    success: function(resp){
                        $.each(resp,function(i,data){
                            let custData = data.customerId+':'+data.nama;
                            $('#customerList').append(`
                            <option>${data.customerId}:${data.nama}</option>
                            `)
                        })
                    }
                })
            }
        // })
    }

</script>