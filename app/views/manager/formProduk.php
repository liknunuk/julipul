<div class="container">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <h3 class='text-center'>DATA BARANG</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group row">
                <label class="col-md-4">Kode Barang</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="sbarcode" placeHolder="Scan Barcode">
                </div>
            </div>
            <form action="<?=BASEURL;?>manager/setProduk/<?=$data['mod'];?>" method="post" class="form-horizontal">
                <input type="hidden" name="barcode" id="barcode" value="<?=$data['produk']['barcode'];?>">
                <div class="form-group row">
                    <label class="col-md-4">Nama Lengkap Barang</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="barangLong" name="barangLong" value="<?=$data['produk']['barangLong'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Nama Singkat Barang</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="barangShort" name="barangShort" value="<?=$data['produk']['barangShort'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Satuan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="satuan" name="satuan" value="<?=$data['produk']['satuan'];?>">
                    </div>
                </div>
        </div>
        <div class="col-lg-6">
                <div class="form-group row">
                    <label class="col-md-4">Harga Pokok Jual</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="harga0" name="harga0" value="<?=$data['produk']['harga0'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Harga Jual 1</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="harga1" name="harga1" value="<?=$data['produk']['harga1'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Harga Jual 2</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="harga2" name="harga2" value="<?=$data['produk']['harga2'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Harga Eceran</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="eceran" name="eceran" value="<?=$data['produk']['eceran'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4">Diskon</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="diskon" name="diskon" value="<?=$data['produk']['diskon'];?>">
                    </div>
                </div>
                <div class="form-group row float-right">
                    <input type="submit" value="Simpan" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- barcode,barangShort,barangLong,satuan,harga0,harga1,harga2,eceran,diskon -->
<?php $this->view('template/bs4js');?>
<script>
$(document).ready( function(){
    $("#sbarcode").focus();
    checkBarcode();
})

function checkBarcode() {
  setInterval(function(){ 
      let barcode = $("#sbarcode").val();
      $("#barcode").val(barcode);
      if(barcode.length > 0 && $("#sbarcode").is(':focus')  ){
          $("#barangLong").focus();
      }
    }, 1000);
}
</script>
