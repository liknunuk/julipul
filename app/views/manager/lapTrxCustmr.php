<div class="container-fluid">
    <div class="row mt-3 mb-3">
        <div class="col-lg-6">
            <h2>Transaksi per Pelanggan <small>[<?=$data['tgl'];?>]</small></h2>
        </div>
        <div class="col-lg-3 form-group row">
            <label for="bulan" class='col-sm-4'>Bulan</label>
                <div class='col-sm-8'>
                    <select id="lapBulan" class="form-control">
                        <?php
                            $th = date('Y');
                            $bul = ['','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agt','Sep','Okt','Nop','Des'];
                            foreach( range(1,12) as $bulan){
                                echo "<option value='$th-".sprintf('%02d',$bulan)."'>".$bul[$bulan]." ".$th."</option>";
                            }
                        ?>
                    </select>
                </div>
        </div>
        <div class="col-lg-3 form-group row">
            <label for="lapTanggal" class="col-sm-4">Tanggal</label>
                <div class="col-sm-8">
                    <input type="date" id="lapTanggal" class="form-control" value="<?=date('Y-m-d');?>">
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
        <!--  nama| totalItem | totalPrice | totalDiskon | totalLaba -->
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Total Item Barang</th>
                        <th>Total Belanja</th>
                        <th>Total Diskon</th>
                        <th>Akumulasi Laba</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['trx'] as $trx): ?>
                    <tr>
                        <td><?=$trx['nama'];?></td>
                        <td class='text-right'><?=$trx['totalItem'];?></td>
                        <td class='text-right'><?=number_format($trx['totalPrice'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($trx['totalDiskon'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($trx['totalLaba'],0,',','.');?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <?php
            if($data['hal'] == 1){
                $hl = 1; $hb = $data['hal'] + 1;
            }else{
                $hl = $data['hal'] - 1; $hb = $data['hal'] + 1;
            }
        ?>
        <div class="col-lg-12">
            <nav aria-label="Page navigation example" class="float-right mr-3">
                <ul class="pagination">

                    <li class="page-item">
                    <a class="page-link" href="<?=BASEURL;?>Manager/lapoGan/<?=$data['tgl'];?>/<?=$hl;?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#"><?=$data['hal'];?></a></li>
                    <a class="page-link" href="<?=BASEURL;?>Manager/lapoGan/<?=$data['tgl'];?>/<?=$hb;?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>

                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js');?>
<script>
$("#lapBulan").change( function(){
    let bulan = $(this).val();
    window.location='<?=BASEURL;?>Manager/lapoGan/'+bulan+'/<?=$data['hal'];?>';
})

$("#lapTanggal").change( function(){
    let tanggal = $(this).val();
    window.location='<?=BASEURL;?>Manager/lapoGan/'+tanggal+'/<?=$data['hal'];?>';
})
</script>