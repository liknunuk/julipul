<div class="container-fluid">
    <div class="row mt-3 mb-3">
        <div class="col-lg-6">
            <h2>Ikhtisar Laba Harian <small>[<?=$data['bulan'];?>]</small></h2>
        </div>
        <div class="col-lg-3 form-group row">
            <label for="bulan" class='col-sm-4'>Bulan</label>
                <div class='col-sm-8'>
                    <select id="lapBulan" class="form-control">
                        <?php
                            $th = date('Y');
                            $bul = ['','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agt','Sep','Okt','Nop','Des'];
                            foreach( range(1,12) as $bulan){
                                echo "<option value='$th-".sprintf('%02d',$bulan)."'>".$bul[$bulan]." ".$th."</option>";
                            }
                        ?>
                    </select>
                </div>
        </div>
        <div class="col-lg-3 form-group row">
            <!-- <label for="lapTanggal" class="col-sm-4">Tanggal</label>
                <div class="col-sm-8">
                    <input type="date" id="lapTanggal" class="form-control" value="<?=date('Y-m-d');?>">
                </div> -->
        </div>
    </div>
    <div class="row">
    <!--  tanggal  | modal  | omset       | akumulasiLaba -->
        <div class="col-lg-12 table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Modal Keluar</th>
                        <th>Total Omset</th>
                        <th>Akumulasi Laba</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $modal = 0 ; $omset = 0; $laba = 0;
                    foreach($data['ikhtisar'] as $ikhtisar): 
                ?>
                    <tr>
                        <td><?=$ikhtisar['tanggal'];?></td>
                        <td class='text-right'><?=number_format($ikhtisar['modal'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($ikhtisar['omset'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($ikhtisar['akumulasiLaba'],0,',','.');?></td>
                    </tr>
                <?php 
                    $modal +=$ikhtisar['modal'];
                    $omset +=$ikhtisar['omset'];
                    $laba  +=$ikhtisar['akumulasiLaba' ];
                    endforeach; 
                ?>
                    <tr>
                        <td>Jumlah</td>
                        <td class="text-right"><?=number_format($modal,0,',','.');?></td>
                        <td class="text-right"><?=number_format($omset,0,',','.');?></td>
                        <td class="text-right"><?=number_format($laba,0,',','.');?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php $this->view('template/bs4js');?>
<script>
$("#lapBulan").change( function(){
    let bulan = $(this).val();
    window.location='<?=BASEURL;?>Manager/ikhtisar/'+bulan;
})

</script>