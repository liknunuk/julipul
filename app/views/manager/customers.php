<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="my-3">Daftar Pelanggan</h2>
            <?php Alert::sankil(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="<?=BASEURL;?>manager/setCustomer/<?=$data['mod'];?>" method="post" class="form-inline">
                <label class="sr-only" for="customerId">Nomor Pelanggan</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">Nomor</div>
                    </div>
                    <input type="text" class="form-control" name="customerId" id="customerId" placeholder="Nomor Pelanggan" readonly value="<?=$data['customer']['customerId'];?>" >
                </div>

                <label class="sr-only" for="nama">Nama Pelanggan</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">Nama</div>
                    </div>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Pelanggan" value="<?=$data['customer']['nama'];?>">
                </div>
                <input type="submit" value="Simpan" class="btn btn-success mx-2">
            </form>
        </div>
    </div>

    <div class="row mt-3">
        <?php foreach($data['customers'] as $cust ): ?>
        <div class="col-sm-3">
            <span class='softblock'>
            <a href="<?=BASEURL;?>Manager/customers/<?=$data['hal'];?>/chg/<?=$cust['customerId'];?>"><?=$cust['nama'];?></a>
            <span class="hapus float-right text-center" id="<?=$cust['customerId'];?>">x</span>
            </span>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <?php
            if($data['hal'] == 1){
                $hl = 1; $hb = $data['hal'] + 1;
            }else{
                $hl = $data['hal'] - 1; $hb = $data['hal'] + 1;
            }
        ?>
        <div class="col-lg-12">
            <nav aria-label="Page navigation example" class="float-right mr-3">
                <ul class="pagination">

                    <li class="page-item">
                    <a class="page-link" href="<?=BASEURL;?>Manager/customers/<?=$hl;?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#"><?=$data['hal'];?></a></li>
                    <a class="page-link" href="<?=BASEURL;?>Manager/customers/<?=$hb;?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>

                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('.hapus').click( function(){
        let custId = $(this).prop('id');

        let tenan = confirm('Pelanggan akan dihapus !');
        if (tenan == true){
            $.post("<?=BASEURL;?>Manager/pecatCustomer",{customerId:custId},
            function(resp){
                if(resp=='1'){ location.reload(); }
                // console.log('Response:',resp);
            })
        }
    })
</script>