<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <h2 class='my-3'>Daftar Barang</h2>
            <?php Alert::sankil(); ?>
            <div class="row my-3">
                <div class="col-sm-8">
                    <a href="<?=BASEURL?>manager/formProduk/new" class="btn btn-primary" id="addProduct">+ Barang</a>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="barangLong" placeholder="Cari Nama Barang">
                </div>
            </div>

            <table class="table table-small">
                <thead>
                    <tr>
                        <th>Kode Barang</th>
                        <th>Nama Lengkap</th>
                        <th>Nama Singkat</th>
                        <th>Satuan</th>
                        <th>Harga Pokok</th>
                        <th>Harga Jual 1</th>
                        <th>Harga Jual 2</th>
                        <th>Harga Eceran</th>
                        <th>Diskon(%)</th>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody id="ProductList">
                    <?php foreach($data['produk'] AS $produk): ?>
                    <tr>
                        <td><?=$produk['barcode'];?></td>
                        <td><?=$produk['barangLong'];?></td>
                        <td><?=$produk['barangShort'];?></td>
                        <td><?=$produk['satuan'];?></td>
                        <td class='text-right px-3'><?=number_format($produk['harga0'],0,',','.');?></td>
                        <td class='text-right px-3'><?=number_format($produk['harga1'],0,',','.');?></td>
                        <td class='text-right px-3'><?=number_format($produk['harga2'],0,',','.');?></td>
                        <td class='text-right px-3'><?=number_format($produk['eceran'],0,',','.');?></td>
                        <td class='text-right px-3'><?=$produk['diskon'];?></td>
                        <td>
                            <a href="<?=BASEURL;?>manager/formProduk/chg/<?=$produk['barcode'];?>" >Edit</a> | 
                            <a href="#" class="hapusProduk" id="<?=$produk['barcode'];?>"> Hapus</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-6">
                    <a href="<?=BASEURL?>manager/formProduk/new" class="btn btn-primary" id="addProduct">+ Barang</a>
                </div>
                <div class="col-sm-6">
                <?php
                    if($data['hal'] == 1){
                        $hl = 1; $hb = $data['hal'] + 1;
                    }else{
                        $hl = $data['hal'] - 1; $hb = $data['hal'] + 1;
                    }
                ?>
                    <nav aria-label="Page navigation example" class="float-right mr-3">
                        <ul class="pagination">

                            <li class="page-item">
                            <a class="page-link" href="<?=BASEURL;?>Manager/produk/<?=$hl;?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#"><?=$data['hal'];?></a></li>
                            <a class="page-link" href="<?=BASEURL;?>Manager/produk/<?=$hb;?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>

                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js');?>
<script>
$(document).ready( function(){
    $(".hapusProduk").click( function(){
        let barcode = $(this).prop('id');
        let yaqeen = confirm('Data akan dihapus!');
        if( yaqeen == true ){
            $.post('<?=BASEURL;?>manager/hapusProduk',{
                barcode: barcode
            },function(resp){
                // location.reload();
                if( resp == '1' ){
                    location.reload();
                }
            });
        }
    })

    $("#barangLong").keyup( function(){
        if( $(this).val().length == 0 ) {location.reload();}
        let product = $(this).val().replace(/ /g, "-");
        $.ajax({
            dataType: 'json',
            url: '<?=BASEURL;?>Manager/cariProduk/'+product,
            success: function(products){
                // console.log(products);
                $("#ProductList tr").remove();
                $.each(products,function(i,data){
                    // console.log(data);
                    $("#ProductList").append(`
                    <tr>
                        <td>${data.barcode}</td>
                        <td>${data.barangLong}</td>
                        <td>${data.barangShort}</td>
                        <td>${data.satuan}</td>
                        <td class='text-right px-3'>${data.harga0}</td>
                        <td class='text-right px-3'>${data.harga1}</td>
                        <td class='text-right px-3'>${data.harga2}</td>
                        <td class='text-right px-3'>${data.eceran}</td>
                        <td class='text-right px-3'>${data.diskon}</td>
                        <td>
                            <a href="<?=BASEURL;?>manager/formProduk/chg/${data.barcode}" >Edit</a> | 
                            <a href="#" class="hapusProduk" id="${data.barcode}"> Hapus</a>
                        </td>
                    </tr>
                    `)
                })
            }
        });
    })
})
</script>