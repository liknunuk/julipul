<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
<!-- container navbar -->
<!-- Contoh Navbar, termasuk dengan doropdown -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?=BASEURL;?>Manager">
    <div><p class="h4 active">MANAJEMEN TOKO</p></div>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuXYZ" aria-controls="menuXYZ" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="menuXYZ">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a  href="<?=BASEURL;?>Manager/produk" class="nav-link text-light">Barang</a>
      </li>

      <li class="nav-item">
        <a  href="<?=BASEURL;?>Manager/customers" class="nav-link text-light">Pelanggan</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle active" href="#" id="xxyyzz" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Laporan
        </a>
        <div class="dropdown-menu" aria-labelledby="xxyyzz">
          <a class="dropdown-item" href="<?=BASEURL;?>Manager/lapoTrx">Daftar Transaksi</a>
          <a class="dropdown-item" href="<?=BASEURL;?>Manager/lapoBar">Penjualan Barang</a>
          <a class="dropdown-item" href="<?=BASEURL;?>Manager/lapoGan">Transaksi Pelanggan</a>
          <a class="dropdown-item" href="<?=BASEURL;?>Manager/ikhtisar">Ikhtisar Tranksaksi</a>
          
        </div> 
      </li>
      
    </ul>
    <ul class="navbar-nav mr-0">
    <li class="nav-item">
        <a class="nav-link" href="<?=BASEURL?>Home/logout">
          <i class="fa fa-user"> Logout</i>
        </a>
      </li>
    <div>
  </div>
</nav>

<!-- container navbar -->
    
    </div>
  </div>
</div>
<?php $this->view('template/bs4js');?>