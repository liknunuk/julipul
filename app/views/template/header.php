<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <link rel="shortcut icon" href="<?=BASEURL;?>img/koin.jpg" type="image/x-icon">
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?=BASEURL;?>/img/logo-idi.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="<?=BASEURL?>/css/bootstrap.min.css">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="<?=BASEURL?>/css/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="<?=BASEURL?>/css/font-awesome.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'/css/saepul.css'; ?>" >
  <!-- Custom Script u/ restful API -->
  <!--script> var pusdata = "< ?=PUSDATA;?>"; var baseurl = "< ?=BASEURL;?>"; </script-->
</head>
<body>
