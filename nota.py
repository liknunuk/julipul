#!/bin/python3

import json
import urllib.request
import math
import sys

#Download the data
trxId = sys.argv[1]
url = 'http://likgroho.mugeno.org/julipul2/public/Toko/nota2/'+trxId
data = urllib.request.urlopen(url).read().decode()

# parse json object
obj = json.loads(data)

# output some object attributes
# print(obj)
# {'info': {'trxNumber': '20060005', 'tanggal': '2020-06-08', 'customerId': '0098', 'nama': 'Ferland Mendy'}, 'data': [{'trxNumber': '20060005', 'trxIndex': '00000029', 'barcode': '9876543201', 'quantity': '5', 'givenPrice': '21280', 'givenDisc': '5', 'barang': 'Bantes 01', 'produk': 'Barang Tes 01', 'satuan': 'pcs'}, {'trxNumber': '20060005', 'trxIndex': '00000030', 'barcode': '9876543202', 'quantity': '4', 'givenPrice': '20700', 'givenDisc': '0', 'barang': 'Bantes 02', 'produk': 'Barang Tes 02', 'satuan': 'pcs'}, {'trxNumber': '20060005', 'trxIndex': '00000031', 'barcode': '9876543210', 'quantity': '3', 'givenPrice': '17250', 'givenDisc': '5', 'barang': 'Bantes 10', 'produk': 'Barang Tes 10', 'satuan': 'pcs'}, {'trxNumber': '20060005', 'trxIndex': '00000032', 'barcode': '9876543220', 'quantity': '10', 'givenPrice': '19040', 'givenDisc': '10', 'barang': 'Bantes 20', 'produk': 'Barang Tes 20', 'satuan': 'pcs'}]}

print("No. Trx :",obj['info']['trxNumber'])
print("Tanggal :",obj['info']['tanggal'])
print("Plnggan :",obj['info']['nama'],obj['info']['customerId'])

totalPrice = 0
totalDscnt = 0

# print( type(obj['data']))
for i in obj['data']:
    harga = int(i['givenPrice'])
    harga = f"{harga:,}".replace(',', '.')
    print(i['quantity'],i['satuan'],i['barang'],':@',harga)
    subTotal0 = (int(i['givenPrice']) * int(i['quantity']) )
    subTotal = f"{subTotal0:,}".replace(',', '.')
    print (str(subTotal).rjust(30))
    totalPrice+=int(subTotal0)
    totalDscnt+=math.ceil(int(i['givenDisc']) / 100 * int(i['quantity']) * int(i['givenPrice']))

print('Total harga')
print(str(f"{totalPrice:,}".replace(',','.')).rjust(30))
print('Diskon')
print(str(f"{totalDscnt:,}".replace(',','.')).rjust(30))
tagihan = totalPrice - totalDscnt
print('Tagihan')
print(str(f"{tagihan:,}".replace(',','.')).rjust(30))
print('')
print('')
print('')
print('')
print('')
print('')
